import React from 'react'

import styles from './styles.scss'

export default () => (
  <header className = {styles.root}>
    <div className = {styles.content}>
      <h1 className = {styles.title}>Genbook</h1>
    </div>
  </header>
)
