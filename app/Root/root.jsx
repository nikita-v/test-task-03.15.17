import React from 'react'
import { Route, Redirect } from 'react-router'

import RootHeader from 'app/Root/Header'
import RootFooter from 'app/Root/Footer'
import Booking from 'app/Booking'
import styles from './styles.scss'

const Root = () => (
  <div>
    <RootHeader />

    <main className = {styles.content}>
      <Route exact path = '/'>
        <Redirect to = '/booking' />
      </Route>

      <Route path = '/booking' component = {Booking} />
    </main>

    <RootFooter />
  </div>
)

export default Root
