import React from 'react'

import styles from './styles.scss'

export default () => (
  <footer className = {styles.root}>
    <div className = {styles.content}>
      <h3>Genbook</h3>

      <span>
        Hassle-free online appointment scheduling software for your
        small business
      </span>
    </div>
  </footer>
)
