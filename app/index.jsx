import React from 'react'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import {
  ConnectedRouter, routerReducer as router, routerMiddleware,
} from 'react-router-redux'
import createBrowserHistory from 'history/createBrowserHistory'
import { render } from 'react-dom'

import booking from 'app/Booking/store'

import Root from 'app/Root'

render(
  <Provider
    store = {createStore(
      combineReducers({
        router,
        booking,
      }),
      (
        applyMiddleware(routerMiddleware(createBrowserHistory())),
        window.__REDUX_DEVTOOLS_EXTENSION__
        && window.__REDUX_DEVTOOLS_EXTENSION__()
      ),
    )}
  >
    <ConnectedRouter history = {createBrowserHistory()}>
      <Root />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
)
