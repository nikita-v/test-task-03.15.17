import React, { PropTypes } from 'react'
import { Link } from 'react-router-dom'

import styles from './styles.scss'

const tabs = [
  { id: 1, title: 'Book Appointment', url: '/booking/book-appointment' },
  { id: 2, title: 'Reviews', url: '/booking/reviews' },
  { id: 3, title: 'Offers', url: '/booking/offers' },
]

const BookingNavigation = ({ pathname }) => (
  <ul className = {styles.root}>
    {tabs.map((tab) => (
      <li
        key = {tab.id}
        className = {styles[`tab${tab.url === pathname ? 'Active' : ''}`]}
      >
        <Link to = {tab.url}>{tab.title}</Link>
      </li>
    ))}
  </ul>
)

BookingNavigation.propTypes = {
  pathname: PropTypes.string.isRequired,
}

export default BookingNavigation
