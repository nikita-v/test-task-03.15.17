import React from 'react'

import styles from './styles.scss'

export default () => (
  <header className = {styles.root}>
    <img
      alt = 'The test logo'
      src = 'http://www.genbook.com/bookings/serviceprovider/30101903/logo'
      className = {styles.logo}
    />

    <div>
      <h2 className = {styles.title}>Angela&apos;s Photography</h2>

      <span>Other info...</span>
    </div>
  </header>
)
