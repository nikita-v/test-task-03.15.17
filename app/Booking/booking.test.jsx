import React from 'react'
import { shallow } from 'enzyme'

import Booking from './booking'
import BookingHeader from './Header'
import BookingNavigation from './Navigation'
import BookingSidebar from './Sidebar'

describe('<Booking />', () => {
  let wrapper

  beforeEach(() => (wrapper = shallow(<Booking
    match = {{ url: '/' }}
    location = {{ pathname: '/' }}
  />)))

  it('the wrapper is div.root', () => {
    expect(wrapper.is('div.root')).toEqual(true)
  })

  it('should render <BookingHeader />', () => {
    expect(wrapper.find(BookingHeader)).toHaveLength(1)
  })

  it('should render <BookingNavigation />', () => {
    expect(wrapper.find(BookingNavigation)).toHaveLength(1)
  })

  it('should render div.main and div.sidebar in a div.content', () => {
    const content = wrapper.find('div.content')

    expect(content.exists()).toBe(true)
    expect(content.find('div.main')).toHaveLength(1)
    expect(content.find('div.sidebar')).toHaveLength(1)
  })

  it('should render <BookingSidebar /> in a sidebar', () => {
    expect(wrapper.find('div.sidebar').find(BookingSidebar)).toHaveLength(1)
  })
})
