import React, { PropTypes } from 'react'
import { Route, Redirect } from 'react-router'

import BookingHeader from 'app/Booking/Header/index'
import BookingNavigation from 'app/Booking/Navigation'
import BookingSidebar from 'app/Booking/Sidebar'
import BookAppointment from 'app/Booking/BookAppointment'
import Reviews from 'app/Booking/Reviews'
import Offers from 'app/Booking/Offers'
import styles from './styles.scss'

const Booking = ({ match, location }) => (
  <div className = {styles.root}>
    <BookingHeader />

    <BookingNavigation pathname = {location.pathname} />

    <div className = {styles.content}>
      <div className = {styles.main}>
        <Route path = {match.url}>
          <Redirect to = {`${match.url}/book-appointment`} />
        </Route>

        <Route
          path = {`${match.url}/book-appointment`}
          component = {BookAppointment}
        />

        <Route
          path = {`${match.url}/reviews`}
          component = {Reviews}
        />

        <Route
          path = {`${match.url}/offers`}
          component = {Offers}
        />
      </div>

      <div className = {styles.sidebar}>
        <BookingSidebar />
      </div>
    </div>
  </div>
)

Booking.propTypes = {
  match: PropTypes.shape({ url: PropTypes.string.isRequired }).isRequired,
  location: PropTypes.shape({ pathname: PropTypes.string }).isRequired,
}

export default Booking
