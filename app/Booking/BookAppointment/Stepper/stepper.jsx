import React, { PropTypes } from 'react'

import styles from './styles.scss'

const steps = [
  { id: 0, title: 'Service & Staff' },
  { id: 1, title: 'Date & Time' },
  { id: 2, title: 'Your Details' },
]

const BookAppointmentStepper = ({ step, changeStep }) => (
  <ul className = {styles.root}>
    {steps.map((stepItem) => (
      <li
        key = {stepItem.id}
        className = {styles[`step${stepItem.id === step ? 'Active' : ''}`]}
      >
        <button onClick = {() => changeStep(stepItem.id)}>
          {stepItem.title}
        </button>
      </li>
    ))}
  </ul>
)

BookAppointmentStepper.propTypes = {
  step: PropTypes.number.isRequired,
  changeStep: PropTypes.func.isRequired,
}

export default BookAppointmentStepper
