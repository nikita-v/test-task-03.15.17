import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { changeStep } from 'app/Booking/store'
import BookAppointment from './book-appointment'

export default connect(
  (state) => (state.booking),
  (dispatch) => bindActionCreators({ changeStep }, dispatch),
)(BookAppointment)
