import React, { PropTypes } from 'react'

import BookAppointmentStepper from './Stepper'
import ServiceStaff from './ServiceStaff'
import DateTime from './DateTime'
import YourDetails from './YourDetails'

const steps = [
  <ServiceStaff />,
  <DateTime />,
  <YourDetails />,
]

const BookingBookAppointment = ({ step, changeStep }) => (
  <div>
    <BookAppointmentStepper step = {step} changeStep = {changeStep} />

    {steps[step]}

    {step + 1 < steps.length &&
      <button onClick = {() => changeStep(step + 1)}>Go to next step</button>
    }
  </div>
)

BookingBookAppointment.propTypes = {
  step: PropTypes.number.isRequired,
  changeStep: PropTypes.func.isRequired,
}

export default BookingBookAppointment
