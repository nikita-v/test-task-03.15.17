const CHANGE_STEP = 'booking/CHANGE_STEP'

export default (state = {
  step: 0,
}, action) => {
  switch (action.type) {
  case CHANGE_STEP:
    return { ...state, step: action.payload }
  default:
    return state
  }
}

export const changeStep = (step) => ({ type: CHANGE_STEP, payload: step })
