const { resolve } = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry: [
    './index.jsx',
  ],
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
    alias: {
      app: resolve(__dirname, 'app'),
    },
  },
  context: resolve(__dirname, 'app'),
  devtool: 'inline-source-map',
  devServer: {
    contentBase: resolve(__dirname, 'dist'),
    publicPath: '/',
    stats: 'errors-only',
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: [/\.js$/, /\.jsx$/],
        use: 'babel-loader',
        include: /app/,
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          use: [
            { loader: 'css-loader',
              options: { modules: true, camelCase: true },
            },
            {
              loader: 'sass-loader',
            },
          ],
        }),
        include: /app/,
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('styles.css'),
  ],
}
